# Guide d'installation Arch Linux

[Le Guide du Grand Monarque!](./INSTALLATION_ARCHLINUX.md)

# Installation de Goodies

[YAY (AUR Helper)](./goodies/yay.md)

# Installation des environements de bureau

[Cinnamon](./desktop-installs/cinnamon.md)

[XFCE4](./desktop-installs/xfce4.md)

[KDE](./desktop-installs/kde.md)

[Pantheon](./desktop-installs/pantheon.md)
