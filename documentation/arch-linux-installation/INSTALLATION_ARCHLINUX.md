# Installation ArchLinux

Installation Testée et validée avec ArchLinux (64bits) et ArchLinux32 (32bits)

# Changer le clavier

`loadkeys fr`

# Partitioner les disques

Lister les disks/partition: 

`fdisk -l`

Selectionner le disks a installer:

`fdisk /dev/sda`

Creation partition boot:

```
n       => Nouvelle partition
p       => Partition primaire
(default)       => Numero de partition
(default)       => Premier sector
+1G     => 1GB
```

Creation partition swap:

```
n
p
(default)
(default)
+1G         => A changer celon les besoin de swap
```

Creation partition system

```
n
p
(default)
(default)
(default)
```

Write changes:

`w`

Formatter les partition (sda1 = boot, sda3 = system):

`mkfs.fat -F32 /dev/sda1`

`mkswap /dev/sda2`

`mkfs.btrfs /dev/sda3`

On active le swap:

`swapon /dev/sda2`

# Monter le system et installer les pre-requis

Monter le system:

`mount /dev/sda3 /mnt`

Installer la base:

`pacstrap /mnt base linux linux-firmware nano`

Generer le fichier fstab:

`genfstab -U /mnt >> /mnt/etc/fstab`

# chroot et nommage du PC et autre configuration

on chroot:

`arch-chroot /mnt`

On renomme son systeme (On met le nom du system sur une ligne sur ce fichier):

`nano /etc/hostname`

On edit le fichier hosts:

`nano /etc/hosts`

```
# Format du fichier:
127.0.0.1       localhost
::1             localhost
127.0.1.1       <hostname-choisi>
```

# Installation des paquets & finalisation de l'install

Grub et EFIBootMgr

`pacman -S grub efibootmgr grub-bios`

Creation dossier boot

`mkdir /boot/efi`

Mount de la partition de boot

`mount /dev/sda1 /boot/efi`

Installation de grub

`grub-install /dev/sda`

Creation du mot de pass root

`passwd`

Generation fichier initramfs

`mkinitcpio -p linux`

On genere le fichier grub:

`grub-mkconfig -o /boot/grub/grub.cfg`

# Installation client DHCP

On install les paquet requis

`pacman -S dhclient dhcpcd`

On active le deamon DHCP (et on peut le start aussi, pour pas avoir a reboot)

`systemctl [enable|start] dhcpcd`

Si tout c'est bien passé, on devrais avoir une IP et accès a internet au prochain reboot (plus loins dans le tutoriel)

# Installation de paquet recommandé

Paquet facultatifs, a adapter celon l'installation a faire

`pacman -S sudo xdg-user-dirs`

# On peut reboot

On quitte le chroot

`exit`

Puis on reboot (après avoir enlever le media d'installation):

`reboot`

# Pre-requis post-install

On change la langue du clavier pour la session actuelle:

`loadkeys fr`

Configuration de la date et de l'heure

`ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime`

Generer la date systeme:

`hwclock --systohc`

Edition des Locales (layouts clavier et langues)

`nano /etc/locale.gen`

Decommenter la ligne fr_FR et lancer la commande

`locale-gen`

Editer le fichier de locale:

`nano /etc/locale.conf`

```
LANG=fr_FR.UTF-8
```

Editer le fichier des clavier:

`nano /etc/vconsole.conf`

```
KEYMAP=fr
```

# Creation d'un nouvel utilisateurs & possibilité de sudo

On crée l'utilisateur:

`useradd <user> -m -U`

```
-m => Créer le home de l'user dans /home/<user>
-U => Créer un groupe avec le nom de l'utilisateur et l'ajoute dedans
```

On lui donne un mot de passe:

`passwd <user>`

On l'ajoute au groupe sudo (wheel)

`usermod -aG wheel <user>`

```
-a => append (ajoute groupe en plus)
-G => liste de groupe
wheel => Groupe pour avoir accès à sudo
```

On autorise au groupe wheel d'utiliser sudo

`nano /etc/sudoers`

Decommenter la ligne `%wheel ALL=(ALL) ALL`

On crée les dossiers du /home de l'utilisateur:

(Pour ca, ilfaut etre logué en votre utilisateurs):

`xdg-user-dirs-update`

