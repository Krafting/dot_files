# Comment installer Pantheon (ElementaryOS Desktop)

## Ne pas suivre ce tutoriel, il ne marche pas!

Avant de commencer, bien installer [yay](https://gitlab.com/Krafting/dot_files) (pour installer les paquets de l'AUR)

On installe les paquets requis:

```
pacman -S --needed pantheon lightdm-pantheon-greeter sound-theme-elementary switchboard lightdm-gtk-greeter elementary-icon-theme elementary-wallpapers pantheon-applications-menu wingpanel-indicator-session wingpanel-indicator-datetime
```

On install Xorg et lightdm:

```
pacman -S --needed xorg lightdm
```
