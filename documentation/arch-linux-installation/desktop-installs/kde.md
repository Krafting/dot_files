# Installation de KDE

[EN COURS, NE PAS UTILISER]

Installation de l'environnement de bureau KDE Plasma

```
pacman -S plasma plasma-wayland-session
```

Laisser toute les options par défault (appuyer sur entrée a chaque fois qu'on demande quelque chose)

Pour les utilisateurs NVIDIA, installer ce paquet (passer a la prochaine etape si vous avez une carte graphique Intel, AMD ou autre)

```
pacman -S egl-wayland
```

Installation des paquets d'applications:

Si vous voulez toute les applications KDE:


```
pacman -S kde-applications-meta
```

Sinon, choisisez les catégories d'applications que vous désirez:

```
pacman -S 
    kde-accessibility-meta
    kde-education-meta
    kde-games-meta
    kde-graphics-meta
    kde-multimedia-meta
    kde-network-meta
    kde-pim-meta
    kde-sdk-meta
    kde-system-meta
    kde-utilities-meta
    kdevelop-meta
```
