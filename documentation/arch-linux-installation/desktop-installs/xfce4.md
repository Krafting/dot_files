# Installation de XFCE 4

# Basique

Pour l'installation basique de XFCE4 il suffit de faire: 

```
pacman -S xfce4 xorg lightdm lightdm-gtk-greeter 
```

Puis on active le Display Manager:

```
systemctl enable lightdm
```

Après un petit Reboot, on sera en graphique!

# Bonus

Si on veut l'experience complète de XFCE4 on peut installer les goodies

```
pacman -S xfce4-goodies
```
