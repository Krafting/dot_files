# Installation environement de bureau (Cinnamon)

```
pacman -S cinnamon gnome-terminal lightdm xorg lightdm-gtk-greeter
```

Laisser toutes les options par defaut (si on vous demande quelque chose, appuyer sur [Entrée])

On active le deamon LightDM

```
systemctl enable lightdm
```
Quand on reboot on devrais etre en graphique

```
reboot
```
