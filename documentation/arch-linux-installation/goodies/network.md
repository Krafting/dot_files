# La commande `ip`

[ENCOURS]

On UP notre carte reseau (on peut regarder le nom de l'interface avec `ip a`)

`ip link set [enp0s3] up`

On execute la commande pour avoir une IP en DHCP:

`dhclient`

Si tout c'est bien passer, on devrais avoir une bonne IP et des bons serveurs DNS dans `/etc/resolv.conf`
