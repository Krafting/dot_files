# Installer YAY!

Installer les dependances:

```
pacman -S --needed git base-devel
```

On change d'utilisateur (Installation en tant que root déconseillé):

```
su <user> && cd ~
```

On télécharge les sources de yay:

```
git clone https://aur.archlinux.org/yay.git
```

On se place dans le dossier:

```
cd yay
```

On construit le paquet depuis les sources:

```
makepkg -si
```
