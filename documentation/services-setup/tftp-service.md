# How to create a basic TFTP server

## On Fedora

Install required package

`sudo dnf install tftp-server`

Edit the config to add some options

`sudo nano /usr/lib/systemd/system/tftp.service`

Edit the line `ExecStart` to

`ExecStart=/usr/sbin/in.tftpd -s -c /var/lib/tftpboot`

Restart systemd and the service

`sudo systemctl daemon-reload && sudo systemctl restart tftp`

A TFTP server should be available, you can put files in `/var/lib/tftpboot/`
