# Notes

### ATTENTION: ENCRYPTER UN DISQUE VA EFFACER TOUT SON CONTENU, FAITES DES BACKUPS AVANT!

Note: La plus par des commandes nécésitent des droits Root, à lancer avec `sudo` si vous n'êtes pas Root

* Tester sur:
    * EndeavourOS avec Cinnamon
    * EndeavourOS avec GNOME

# Setup

Verifier que cryptsetup est bien installé:

```
cryptsetup --version
```

Si la commande n'est pas trouvée, installer le paquet:

```
pacman -S cryptsetup
```

# Encrypter le disque 

Je conseil d'effacer toute les partitions sur le disque avant de continuer.

ATTENTION: N'oubliez pas de démonter toute les partitions qui sont montée (à faire avec gnome-disks par example, via le bouton STOP de chaque partition)

Verifier le nom du disque que l'on veux encrypter:

```
lsblk
```

On encrypte le disque (Remplacer sdX par le bon disque):

```
cryptsetup --verbose --verify-passphrase luksFormat /dev/sdX
```

Un WARNING apparrait vous disant que toute les données seront effacer, pour continuer taper "YES" dans le champ

Puis après on va vous demander une Passphrase, ATTENTION, ne la perdez pas, stocker la dans un endroit sûre

Si on vous affiche "Command Successful" tout s'est bien passé!

Ensuite, on debloque le nouveau disque et on lui donne un nom pour plus de facilités:

```
cryptsetup luksOpen /dev/sdX <NomMapper>
```

Il va vous redemander votre passphrase

Ensuite, on verify que l'on a bien un `/dev/mapper/<NomMapper>` a la fin du fdisk:

```
fdisk -l
```

On format la partition afin de pouvoir y stocker des données:

Remplacer `<UnLabel>` par le nom que vous voulez donner a votre disque

```
mkfs.btrfs -L <UnLabel> /dev/mapper/<NomMapper>
```

Ensuite, on ferme le disque pour pouvoir le retirer:

```
cryptsetup luksClose <NomMapper>
```

Si on debranche, puis rebranche le disque, un pop-up apparaitra, nous demandant notre passphrase, en cliquand sur OK le disque sera monter et apparaitra dans le gestionnaire de fichier!

# Les droits!

Petit probleme, nous n'avons pas les droits d'écrire sur le disque avec un utilisateur autre que ROOT

Pour y corriger, brancher votre clé et laisser le systeme la monter.

Trouver le point de montage de la clé avec (se trouve souvant dans `/run/media/<YourUser>/<UnLabel>`)

```
lsblk
```

Puis on chmod:

```
chown -R `whoami`:users /path/to/mount/point
```

Et voila! On peut désormais ecrire des fichiers sur le disque!

