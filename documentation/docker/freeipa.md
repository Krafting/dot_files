# Install FreeIPA with Docker 

## on Fedora 36 Linux

Pre-requisite

```
mkdir /domain && mkdir /domain/data
setsebool -P container_manage_cgroup 1
sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
```

Then reboot your system

The Docker on-liner

```
docker run --name freeipa -p 53:53/udp -p 53:53 \
    -p 80:80 -p 443:443 -p 389:389 -p 636:636 -p 88:88 -p 464:464 \
    -p 88:88/udp -p 464:464/udp -p 123:123/udp -ti \
    -h ipa.domain.local --read-only --sysctl net.ipv6.conf.all.disable_ipv6=0 \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    -v /domain/data:/data:Z freeipa/freeipa-server:fedora-36 \
    ipa-server-install -U -r DOMAIN.LOCAL -p DS_PASSWORD -a ADMIN_PASSWORD
```
