# Personal Script

This repo is used to store and share my personal scripts, and also have fun/train with the git protocol. Feel free to give suggestions!

# Link to files

[SOFTWARE.md](.krafting/SOFTWARES.md) List of the softwares I use, and some that I could use.

# Download Music File with yt-dlp

This command is for me to use when downloading MP3 of videos:

```shell
/usr/bin/yt-dlp -x --audio-format mp3 --prefer-ffmpeg -o '%(title)s.%(ext)s' --embed-thumbnail [URL]
```

With this command it can use SponsorsBlock to remove non-music in videos (useful in some cases where the intro/outro is really long):

```shell
/usr/bin/yt-dlp -x --audio-format mp3 --prefer-ffmpeg -o '%(title)s.%(ext)s' --embed-thumbnail --sponsorblock-remove music_offtopic,intro,outro [URL]
```

If I need to limit the size of the playlist:

```shell
/usr/bin/yt-dlp -x --audio-format mp3 --prefer-ffmpeg --playlist-end 162 -o '%(title)s.%(ext)s' --embed-thumbnail [URL]
```

# Download Videos with yt-dlp

With this command it can use SponsorsBlock to make segments into chapters in videos :

```shell
/usr/bin/yt-dlp --sponsorblock-mark all [URL]
```

Or remove sponsors and tag everything:


```shell
/usr/bin/yt-dlp --sponsorblock-remove sponsor --sponsorblock-mark all  [URL]
```

# Making tar archives

This command is for me when I want to create archives of my folders

```shell
tar -I pigz -cvf <name-of-archive>.tgz <name-of-folder-or-file>
```

# Using RSYNC to make my backup easier

IMPORTANT: DO NOT forget the / after the source and the destination folders or it will create another Folder with the name of the source, and so, reupload all the data

```shell
rsync -ahvAE --delete --stats  --exclude={'lost+found','.Trash-1000'} <folder-to-backup>/ <destination-of-the-backup>/
```

To use RSYNC over ssh use this

```shell
rsync -ahvAE <folder-to-sync>/ user@9.9.9.9:/<remote-folder/
```

# Rename files with regex

## rename.pl 

For testing purposes:

```shell
rename.pl -ve 's|(.+)E([0-9]+)S([1-4])*.([a-zA-Z0-9]+)$|$1Episode\ $2\ S0$3\.$4|' * 
```

Then, write the changes:

```shell
rename.pl -ve 's|(.+)E([0-9]+)S([1-4])*.([a-zA-Z0-9]+)$|$1Episode\ $2\ S0$3\.$4|' * --apply 
```

## rename.ul

Replace char with another in file name (needs to be run multiple time)

```shell
for i in *; do  mv "$i" "`echo $i | sed 's/＂/"/'`"; done
```

## prename

Don't apply change (remove -n to apply changes):

```shell
prename -n -v 's/(.*)/$1/' *
```

## Remove char from multiple filenames:

Changes applied once it's ran:

```shell
for file in *; do mv "${file}" "${file/CHARTOREMOVE/}"; done
```

# Folders
## .krafting
This is my config folder, where script, logos, icons and other stuff I need regurarely is stored.

## .krafting/scripts
Quick access to my bash script, used in the .zshrc with aliases.


# Files
## install-arch.bash
Quick install for an arch system 
```
Custom options: 
    --software :    Install common softwares that I use
    --cinnamon :    to use when needing to configure the Cinnamon Desktop at my liking
    --gnome :       to use when needing to configure the GNOME Desktop at my liking
```

## install-debian.bash
Quick install for an debian system (Debian, Ubuntu, and derivatives)
```
Custom options: 
    --software :    Install common softwares that I use
    --cinnamon :    to use when needing to configure the Cinnamon Desktop at my liking (for Linux Mint, mainly)
    --gnome :       to use when needing to configure the GNOME Desktop at my liking
```
