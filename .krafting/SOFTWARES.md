### PRESQUE REQUIS ###
* CKB-Next
* Deluge
* Discord
* Element
* Firefox
* Flameshot
* Lutris
* OBS Studio
* OpenRGB
* PulseAudio Volume Control
* Steam
* Visual Studio Codium
* Wine (Staging)
* youtube-dl & yt-dlp



### OPTIONAL ###

* ADB
* Bitwig Studio
* Fragments
* GIMP
* Green With Envy
* Itch
* KDE Connect
* Kid3
* Mixxx
* Nuclear
* Phoronix Test Suite
* PuTTY
* Shortwave
* Signal
* Warpinator