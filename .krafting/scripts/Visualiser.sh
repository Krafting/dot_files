#!/bin/bash
PS3='Please enter your choice: '
options=("Synthwave" "White Up & Down" "Kill all" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Synthwave")
            screen -dmS vis1 glava -d -e rcircle.glsl &&
            screen -dmS vis2 glava -d -e rc.glsl &&
            screen -dmS vis3 glava -d -e rcbars.glsl
            echo "Enjoy your music bro."
            break
            ;;
        "White Up & Down")
            screen -dmS vis1 glava -d -e rc.glsl &&
            screen -dmS vis2 glava -d -e rcbars.glsl
            echo "Enjoy your music bro."
            echo "you chose choice 2"
            ;;
        "Kill all")
            screen -XS vis1 kill &&
            screen -XS vis2 kill &&
            screen -XS vis3 kill
            echo "You killed them all, you monster."
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done