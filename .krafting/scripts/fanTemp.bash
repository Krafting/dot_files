#!/bin/bash
currenttemp=$(sensors | grep -i Tdie | awk {'print $2'} | tr -d +C°)
echo $currenttemp

if [[ "$currenttemp" < 45 ]]
then 
   liquidctl --match Commander set sync speed 0
   echo "Temperature : $currenttemp Speed 0"
elif [[ "$currenttemp" > 45 ]]
then 
   liquidctl --match Commander set sync speed 20
   echo "Temperature : $currenttemp Speed 20"
elif [[ "$currenttemp" > 50 ]]
then 
   liquidctl --match Commander set sync speed 30
   echo "Temperature : $currenttemp Speed 30"
elif [[ "$currenttemp" > 55 ]]
then 
   liquidctl --match Commander set sync speed 40
   echo "Temperature : $currenttemp Speed 50"
elif [[ "$currenttemp" > 60 ]]
then 
   liquidctl -v --match Commander set sync speed 50
   echo "Temperature : $currenttemp Speed 65"
elif [[ "$currenttemp" > 65 ]]
then 
   liquidctl -v --match Commander set sync speed 65
   echo "Temperature : $currenttemp Speed 65"
elif [[ "$currenttemp" > 70 ]]
then 
   liquidctl --match Commander set sync speed 75
   echo "Temperature : $currenttemp Speed 75"
elif [[ "$currenttemp" > 80 ]]
then 
   liquidctl --match Commander set sync speed 100
   echo "Temperature : $currenttemp Speed 0"
fi
