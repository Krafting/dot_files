#!/bin/bash
Version=alpha_0.8.3
Date=22.04.2021
Author="Lanséria & 02030997l"
# TODO :
#   user usernet_perso_iut so we can add multiple username
#
read -d '' helpPage << "_EOF_"
Avant toute chose :
    * Pour lancer ce script utiliser bash: bash IUT_Drives_DebianBased.sh
    * Ne lancez PAS ce script avec sudo
    * Ce script vous connecte automatiquement au VPN, si installé.
    * Les disques montés seront disponible dans /mnt/Perso_IUT; /mnt/Public_IUT et /mnt/Perso_WEB
    * Vous devrez lancer ce script pour remonter vos disques à chaque redémarrage.
    
Les options:
    -h | --help : Affiche le message d'aide
    -v | --version : Afficher la version du script
    -u | --unmount : Démonte les disques réseaux déjà montés
    -d | --disconnect : Se deconnecte du VPN de l'université.
    -c | --connect : Se connecte au VPN.
    (WIP) -n | --new : Connect les disques d'un nouvel utilisateur (Web & Perso seulement)

_EOF_
read -d '' helpPageEXP << "_EOF_"
Les options expérimentales (Ne pas utiliser en production):
    --save : Sauvegarde les identifiants et disques réseaux, 
             utilisez 'sudo mount -a' pour remonter les disques 
             une fois connecté au VPN
    --unsave : Supprime les disques réseau sauvegardés.


Distributions testées (Non-root): Linux Mint 19+, Ubuntu 20+, Arch Linux
_EOF_

# On affiche un message la premiere fois que l'on lance le script
# La bonne utilisation de ce script est essentiel.
if [ ! -f ~/.VPN_script_understood ]; then 
    gotit=0
    while [ $gotit -eq 0 ] 
    do
        echo "$helpPage"
        echo ""
        echo ""
        read -p "Avez vous bien compris le fonctionnement de ce script ? [oui/Non]" isokay
        if [ $isokay = 'oui' ]; then
            echo ""
            echo "Génial alors, vous pouvez désormais l'utiliser !"
            touch ~/.VPN_script_understood
            gotit=1
            exit
        else 
            echo ""
            echo "/!\ Vous devez comprendre comment utiliser ce script correctement avant de l'utiliser :("
            echo "Essayons encore une fois!"
            echo ""
            sleep 5
        fi
        echo ""
    done
fi
case $1 in
    --help | -h)
        echo "$helpPage"
        echo "$helpPageEXP"
        exit
    ;;
    --version | -v)
        echo "IUT Drives ${Version} by ${Author}"
        exit
    ;;
    --disconnect | -d)
        /opt/cisco/anyconnect/bin/vpn disconnect
        # rm ~/.VPN_IUT_CREDS
        if [ $? -eq 0 ]; then
            echo ""
            echo "Le VPN a bien été déconnecté."
            echo ""
        else
            echo ""
            echo "/!\ ERREURS: Le VPN n'a pas pu etre déconnécté."
            echo "/!\ Êtes vous déjà déconnecté du VPN ?"
            echo ""
        fi
        exit
    ;;
    --unmount | -u)
        sudo umount -f /mnt/Perso_IUT
        sudo umount -f /mnt/Public_IUT
        sudo umount -f /mnt/Perso_WEB
        echo ""
        echo "Les disques ont été démontés avec succès."
        echo ""
        exit
    ;;
    *)
    ;;
esac

sudo touch /tmp/.INITIATINGSUDO
sudo rm /tmp/.INITIATINGSUDO
echo "
Il est important d'utiliser ce script correctement
Pour savoir bien utiliser ce script, et pour plus d'options, utiliser l'option --help
"
read -p "Entrez votre nom d'utilisateur de l'IUT: " username

# Delete entry, we only ask for the username. why ? so we don't delete other username home folder (experimental for now)
case $1 in
    --unsave)
        # Always make a backup of the fstab file.
        sudo cp /etc/fstab /etc/fstab$1.autobak
        # Remove old line, to avoid duplicates
        sudo sed -i.bak "\@^//srv-data2.iut-acy.local/Public /mnt/Public_IUT/@d" /etc/fstab
        sudo sed -i.bak "\@^//srv-data2.iut-acy.local/home/etudiants/${username} /mnt/Perso_IUT/@d" /etc/fstab
        echo ""
        echo "Les disques ont été supprimé de vos configurations de disque avec succès."
        echo ""
        exit
    ;;
esac

read -sp "Entrez votre mot de passe de l'IUT : " password

case $1 in
    --connect | -c)
        if [ -d /opt/cisco/anyconnect ]; then
            echo "${username}
${password}
y" > ~/.VPN_IUT_CREDS
            # Se connecte au VPN
            echo "Connexion au VPN en cours..."
            /opt/cisco/anyconnect/bin/vpn connect vpn.univ-smb.fr -s < ~/.VPN_IUT_CREDS
            if [ $? -eq 0 ]; then
                echo "Connexion au VPN établie avec succès"
            else
                echo "Des erreurs sont apparus lors de la tentative de connexion au VPN (Mauvais identifiants ?)"
                exit
            fi
            # On supprime le fichier de credentials
            rm ~/.VPN_IUT_CREDS
            exit
        fi
    ;;
esac
# On verifie si le VPN est installé
if [ -d /opt/cisco/anyconnect ]; then
    # On enregistre les identifiants, le fichier sera supprimé lors de la deconnexion du VPN (si faite par ce script)
    echo "${username}
${password}
y" > ~/.VPN_IUT_CREDS
    # Se connecte au VPN
    echo "Connexion au VPN en cours..."
    /opt/cisco/anyconnect/bin/vpn connect vpn.univ-smb.fr -s < ~/.VPN_IUT_CREDS
    if [ $? -eq 0 ]; then
        echo "Connexion au VPN établie avec succès"
    else
        echo "Des erreurs sont apparus lors de la tentative de connexion au VPN (Mauvais identifiants ?)"
        exit
    fi
    # On supprime le fichier de credentials
    rm ~/.VPN_IUT_CREDS
fi

# Check for cifs-utils // Useful ?
echo ""
dpkg --get-selections | grep cifs-utils || apt-get install cifs-utils

# Check Folders
if [ ! -d /mnt ]; then
    sudo mkdir -v /mnt/
fi
if [ ! -d /mnt/Public_IUT/ ]; then
    sudo mkdir -v /mnt/Public_IUT/
fi
if [ ! -d /mnt/Perso_IUT/ ]; then
    sudo mkdir -v /mnt/Perso_IUT/
fi
if [ ! -d /mnt/Perso_WEB/ ]; then
    sudo mkdir -v /mnt/Perso_WEB/
fi
if [ ! -d /mnt/Projet_WEB/ ]; then
    sudo mkdir -v /mnt/Projet_WEB/
fi

# Mount drives
sudo mount -t cifs //srv-data2.iut-acy.local/Public /mnt/Public_IUT/ -o username=$username,password=$password,domain=iut-acy,nosetuids,noperm
sudo mount -t cifs //srv-data2.iut-acy.local/home/etudiants/$username /mnt/Perso_IUT/ -o username=$username,password=$password,domain=iut-acy,nosetuids,noperm
sudo mount -t cifs //srv-peda.iut-acy.local/WWW/$username /mnt/Perso_WEB/ -o username=$username,password=$password,domain=iut-acy,nosetuids,noperm
sudo mount -t cifs //srv-prj.iut-acy.local/RT/1projet10/ /mnt/Projet_WEB/ -o username=$username,password=$password,domain=iut-acy,nosetuids,noperm
if [ $? -eq 0 ]; then
    echo ""
    echo "Les disques ont été montés avec succès."
    echo "Vous pouvez y accéder depuis le chemin: /mnt"
    echo ""
else
    echo ""
    echo "/!\ ERREURS: Les disques n'ont pas été monté suite à une erreur."
    echo "/!\ Vérifier que vous n'ayez pas fais d'erreurs. (Disques déjà monté ?)"
    echo ""
fi

case $1 in
    --save)
        # Always make a backup of the fstab file.
        sudo cp /etc/fstab /etc/fstab$1.autobak
        # Remove old line, to avoid duplicates
        sudo sed -i.bak "\@^//srv-data2.iut-acy.local/Public /mnt/Public_IUT/@d" /etc/fstab
        sudo sed -i.bak "\@^//srv-data2.iut-acy.local/home/etudiants/${username} /mnt/Perso_IUT/@d" /etc/fstab
        # Add new lines
        echo "//srv-data2.iut-acy.local/home/etudiants/${username} /mnt/Perso_IUT/ cifs username=${username},password=${password},domain=iut-acy 0 0" >> /etc/fstab
        echo "//srv-data2.iut-acy.local/Public /mnt/Public_IUT/ cifs username=${username},password=${password},domain=iut-acy 0 0" >> /etc/fstab
        echo ""
        echo "Les disques ont été sauvegardés avec succès."
        echo ""
    ;;
esac

