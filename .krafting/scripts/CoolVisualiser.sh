#!/bin/bash
# PS3='Please enter your choice: '
# options=("Synthwave" "White Up & Down" "Kill all" "Quit")
case $1 in
    --synthwave)
        screen -dmS vis1 glava -d -e rcircle.glsl &&
        screen -dmS vis2 glava -d -e rc.glsl &&
        screen -dmS vis3 glava -d -e rcbars.glsl
        echo "Enjoy your music, bro."
        break
        ;;
    --white)
        screen -dmS vis1 glava -d -e rc.glsl &&
        screen -dmS vis2 glava -d -e rcbars.glsl
        echo "Enjoy your music, bro."
        ;;
    --coolone)
        screen -dmS vis1 glava -d -e rcDown.glsl &&
        screen -dmS vis2 glava -d -e rc.glsl
        echo "Enjoy your music, bro."
        ;;
    --kill)
        screen -XS vis1 kill &&
        screen -XS vis2 kill &&
        screen -XS vis3 kill
        echo "You killed them all, you monster."
        break
        ;;
    --quit)
        break
        ;;
    *) echo "invalid option";;
esac