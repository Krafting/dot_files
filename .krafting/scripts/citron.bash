
#!/bin/bash
PID=$(pgrep cinnamon-sessio)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)

# HEURE ENTRE 20H et 8H (NUIT)
if (( $(date +"%H") > 17 || $(date +"%H") <= 8 )); then
	gsettings set org.cinnamon.theme name "Arc-Dark"
	gsettings set org.cinnamon.desktop.interface gtk-theme "Arc-Dark"
	gsettings set org.cinnamon.desktop.interface icon-theme "Reversal-blue-dark"
	gsettings set org.gnome.Terminal.ProfilesList default 02e97ab1-e013-4711-b23a-27a8c472dbb7

	#WALLPAPER 
	gsettings set org.cinnamon.desktop.background picture-uri file:///mnt/Data/Pictures/Wallpapers/Linux/arch.png


# HEURE ENTRE 8h et 20H (JOUR)
elif (( $(date +"%H") <= 17 || $(date +"%H") > 8 )); then
	gsettings set org.cinnamon.theme name "Arc"
	gsettings set org.cinnamon.desktop.interface gtk-theme "Arc"
	gsettings set org.cinnamon.desktop.interface icon-theme "Reversal-blue"
	gsettings set org.gnome.Terminal.ProfilesList default 005a9981-6c3a-452c-a396-139f9a3c7883

	#WALLPAPER 
	gsettings set org.cinnamon.desktop.background picture-uri file:///mnt/Data/Pictures/Wallpapers/Linux/arch.png


# UN SINON QUI SERS A R (Oui)
else
	echo "OUI"
fi
