#!/bin/bash
while [ -n "$1" ]; do # while loop starts
	case "$1" in
        --software | -s)
            yes | sudo pacman -Syu
            yes | sudo pacman -S flameshot steam discord arc-gtk-theme audacious celluloid mpv youtube-dl baobab gnome-disk-utility htop kdeconnect lutris virtualbox pavucontrol signal-desktop warpinator audacious-plugins zsh exa bat flatpak glava zsh-syntax-highlighting

            yes | yay -Syu
            yes | yay -S vscodium-bin pamac-all timeshift-bin ulauncher openrb-bin obs-studio-browser reversal-icon-theme-git layan-cursor-theme-git snapd 
            
            yes | flatpak install flathub de.haeckerfelix.Fragments io.lbry.lbry-app io.gitlab.librewolf-community de.haeckerfelix.Shortwave 
        ;;
        --cinnamon | -c)
            yes | sudo pacman -S cinnamon
            echo ""
            echo "Cinnamon Settings are getting applied..."
            gsettings set org.cinnamon.theme name "Arc-Dark"
            gsettings set org.cinnamon.desktop.interface gtk-theme "Arc-Dark"
            gsettings set org.cinnamon.desktop.interface icon-theme "Reversal-blue-dark"
            gsettings set org.cinnamon.desktop.interface cursor-theme "Layan-white-cursors"
            gsettings set org.cinnamon enabled-applets "['panel1:left:0:menu@cinnamon.org:0', 'panel1:left:1:grouped-window-list@cinnamon.org:2', 'panel1:right:1:systray@cinnamon.org:3', 'panel1:right:2:xapp-status@cinnamon.org:4', 'panel1:right:3:notifications@cinnamon.org:5', 'panel1:right:5:removable-drives@cinnamon.org:7', 'panel1:right:6:keyboard@cinnamon.org:8', 'panel1:right:7:favorites@cinnamon.org:9', 'panel1:right:8:network@cinnamon.org:10', 'panel1:right:9:sound@cinnamon.org:11', 'panel1:right:10:power@cinnamon.org:12', 'panel1:right:11:calendar@cinnamon.org:13', 'panel2:left:1:grouped-window-list2@cinnamon.org:14', 'panel2:left:0:menu@cinnamon.org:15', 'panel2:right:2:calendar@cinnamon.org:16', 'panel2:right:1:trash@cinnamon.org:17', 'panel2:right:0:color-picker@fmete:18']"
            gsettings set org.cinnamon enabled-search-providers "['calc@cinnamon.org']"
            gsettings set org.cinnamon panels-height "['1:40', '2:40']"
            gsettings set org.cinnamon panels-enabled "['1:0:right', '2:1:left']"
            gsettings set org.cinnamon panel-zone-icon-sizes "[{"panelId": 1, "left": 32, "center": 0, "right": 24}, {"left": 32, "center": 0, "right": 24, "panelId": 2}]"
            gsettings set org.cinnamon panel-zone-symbolic-icon-sizes "[{"panelId": 1, "left": 28, "center": 28, "right": 18}, {"left": 28, "center": 28, "right": 18, "panelId": 2}]"
            gsettings set org.cinnamon panel-zone-text-sizes "[{"panelId": 1, "left": 0, "center": 0, "right": 13.0}, {"left": 0, "center": 0, "right": 13.0, "panelId": 2}]"
            gsettings set org.cinnamon show-media-keys-osd "small"
            echo "Cinnamon is configured."
        ;;
        --gnome | -g)
            yes | sudo pacman -S gnome
            echo "not yet implemented"
        ;;
        --help | -h | he)
            echo "Some help soon"
            exit
        ;;
        --) 
            break;
            ;;
    esac
    shift
done
# gsettings set org.cinnamon 
# gsettings set org.cinnamon
# gsettings set org.cinnamon
# gsettings set org.cinnamon



# gsettings set org.cinnamon.desktop.background picture-uri file:///mnt/Data/Pictures/Wallpapers/Linux/arch.png

