#request mod dbars

#request setfloating  false
#request setdecorated true
#request setfocused   false
#request setmaximized false

#request setopacity "native"

#request setmirror false

#request setversion 3 3
#request setshaderversion 330

#request settitle "GLava"

#request setgeometry 1680 -1 1920 1080

#request setbg 00000000

#request setxwintype "normal"

// #request addxwinstate "sticky"
// #request addxwinstate "skip_taskbar"
// #request addxwinstate "skip_pager"
// #request addxwinstate "above"
// #request addxwinstate "pinned"

#request setclickthrough false

#request setsource "auto"

#request setswap 0

#request setinterpolate false

#request setframerate 75

#request setfullscreencheck false

#request setprintframes true

#request setsamplesize 1024

#request setbufsize 4096

#request setsamplerate 22050

#request setaccelfft true

/* DEPRECATED */
#request setforcegeometry false
#request setforceraised false
#request setbufscale 1
